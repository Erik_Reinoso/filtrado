import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt

img = cv.imread('pe.jpg')
# Cambio de orden de los canales para lectura
b,g,r = cv.split(img)
img = cv.merge([r,g,b])

#Crea el kernel 5x5 / 25
kernel = np.ones((3,3),np.float32)/9
#Filtra la imagen utilizando el kernel anterior (Convolusion)
dst = cv.filter2D(img,-1,kernel)
#dst1 = cv.filter2D(img,-1,kernel)

#Filtra la imagen utilizando el Filtro Medio
medianBlur = cv.medianBlur(img, 3)

#Filtra la imagen utilizando el Promedio
blur = cv.blur(img,(3,3))

#Filtra la imagen utilizando Filtro Gaussiano
Gblur = cv.GaussianBlur(img,(3,3),0)

#Filtra la imagen utilizando Filtro de Mediana
median = cv.medianBlur(img,3)
#filtro por segundo vez mediana
median2 = cv.medianBlur(median,3)
#filtro por tercera vez mediana
median3 = cv.medianBlur(median2,3)

plt.subplot(231),plt.imshow(img),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(232),plt.imshow(dst),plt.title('Convolución')
plt.xticks([]), plt.yticks([])
plt.subplot(233),plt.imshow(blur),plt.title('Promediada')
plt.xticks([]), plt.yticks([])
plt.subplot(235),plt.imshow(medianBlur),plt.title('Filtro medio')
plt.xticks([]), plt.yticks([])
plt.subplot(234),plt.imshow(Gblur),plt.title('Gaussiano')
plt.xticks([]), plt.yticks([])
plt.subplot(236),plt.imshow(median),plt.title('Mediana')
plt.xticks([]), plt.yticks([])
plt.show()

plt.subplot(221),plt.imshow(img),plt.title('Original')
plt.xticks([]), plt.yticks([])
plt.subplot(222),plt.imshow(median),plt.title('Mediana')
plt.xticks([]), plt.yticks([])
plt.subplot(223),plt.imshow(median2),plt.title('Segunda Mediana')
plt.xticks([]), plt.yticks([])
plt.subplot(224),plt.imshow(median3),plt.title('Tercera Mediana')
plt.xticks([]), plt.yticks([])
plt.show()

